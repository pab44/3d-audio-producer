#ifndef ECHO_ZONE_PROPERTIES_H
#define ECHO_ZONE_PROPERTIES_H

//delay
const float default_delay = 0.1f;
const float min_delay = 0.0f;
const float max_delay = 0.207f;

//lr delay
const float default_lr_delay = 0.1f;
const float min_lr_delay = 0.0f;
const float max_lr_delay = 0.404f;


//damping
const float default_damping = 0.5f;
const float min_damping = 0.0f;
const float max_damping = 0.99f;


//feedback 
const float default_feedback = 0.5f;
const float min_feedback = 0.0f;
const float max_feedback = 1.0f;


//spread
const float default_spread = -0.5f;
const float min_spread = -1.0f;
const float max_spread = 1.0f;


#endif