#ifndef AUDIO_PLAYER_PARAMETERS_H
#define AUDIO_PLAYER_PARAMETERS_H

#define BUFFER_TIME_MS 160 // 200 milliseconds 
#define SMALL_BUFFER_SIZE 256 // samples 
#define MAX_CHANNELS 2

#endif